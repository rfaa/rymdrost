#[derive(Debug)]
pub struct Monster {
    pub x: u8,
    pub y: u8,
    pub dmg: u8,
    pub hp: u8,
    pub shield: u8,
}

impl Monster {
    pub fn new(x: u8, y: u8, dmg: u8, hp: u8, shield: u8) -> Self {
        Monster {
            x,
            y,
            dmg,
            hp,
            shield,
        }
    }

    pub fn dmg(&mut self, dmg: u8) {
        // empty shield first
        if self.shield >= dmg {
            self.shield -= dmg;
        } else {
            // can hp sustain dmg after shield has been emptied?
            if self.hp >= dmg - self.shield {
                self.hp -= dmg - self.shield;
                self.shield = 0;
            // otherwise empty both shield and hp
            } else {
                self.hp = 0;
                self.shield = 0;
            }
        }
    }

    pub fn is_dead(&self) -> bool {
        self.hp == 0
    }
}

mod monster;

use monster::Monster;

fn main() {
    let mut monster = Monster::new(0, 0, 2, 4, 7);
    loop {
        println!("{:?}", monster);
        monster.dmg(3);
        if monster.is_dead() {
            break;
        }
    }

    println!("{:?}", monster);
}
